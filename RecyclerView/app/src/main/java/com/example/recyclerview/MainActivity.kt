package com.example.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

//lateinit var tvTitle : TextView
//lateinit var cbDone : CheckBox

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rvTodos : RecyclerView = findViewById(R.id.rvTodos)
        val etTodo : EditText = findViewById(R.id.etTodo)
        val btnAddTodo : Button = findViewById(R.id.btnAddTodo)

        var todoList = mutableListOf(
            Todo("Follow AndroidDevs", isChecked = false),
            Todo("Learn about RecyclerView", isChecked = true),
            Todo("Feed my cat", isChecked = true),
            Todo("Prank my boss", isChecked = true),
            Todo("Eat some curry", isChecked = true),
            Todo("Take a shower", isChecked = true),
        )

        val adapter = TodoAdapter(todoList)
        rvTodos.adapter = adapter
        rvTodos.layoutManager = LinearLayoutManager(this)

        btnAddTodo.setOnClickListener {
            val title = etTodo.text.toString()
            val todo = Todo(title,false)
            todoList.add(todo)
            adapter.notifyItemInserted(todoList.size-1)
        }
    }
}